import zwoasi
import matplotlib.pyplot as plt
import IPython.display as display
import numpy.typing as npt
import numpy as np
from typing import Literal


class ZWO_Camera:
    """Wrapper for a ZWO ASI camera.

    Attributes:
        camera_number: An integer describing the camera index.
        camera_id: A string of the camera model.
        gain_setting: An integer of the current gain setting.
        exposure_setting: An integer of the current exposure time
            in microseconds.
        image_type: A string of the image colour mode and bit depth.
            e.g. RGB24, RAW8, RAW16
        camera_obj: :module:`zwoasi` camera instance object.
    """

    def __init__(
        self,
        camera_number: int = 0,
        camera_id: str = "ZWO ASI1600MM Pro",
        gain_setting: int = 250,
        exposure_setting: int = 100000,
        image_type: Literal["RGB24", "RAW8", "RAW16"] = "RAW16",
    ):
        """
        Initializes the ZWO ASI camera with specified parameters.

        camera_number: Camera index.
        camera_id: Camera model.
        gain_setting: Gain setting.
        exposure_setting: Exposure time in microseconds.
        image_type: Image colour mode and bit depth.

        """
        library_path = "C:\\Program Files\\ASI SDK\\lib\\x64\\ASICamera2.dll"
        try:
            zwoasi.init(library_file=library_path)
        except zwoasi.ZWO_Error:
            print(f"Could not find '{library_path}'")

        self.camera_id = camera_id
        self.camera_number = camera_number
        self.set_gain_setting(gain_setting)
        self.set_exposure_setting(exposure_setting)  # [us]
        self.set_image_type(image_type)

    def set_image_type(
        self, image_type: Literal["RGB24", "RAW8", "RAW16"]
    ) -> None:
        rgb24_names = ("rgb24", "RGB24", "rgb", "RGB")
        raw8_names = ("raw8", "RAW8", "Raw8")
        raw16_names = ("raw16", "RAW16", "Raw16")

        if image_type in raw8_names:
            self.image_type = zwoasi.ASI_IMG_RAW8
        elif image_type in raw16_names:
            self.image_type = zwoasi.ASI_IMG_RAW16
        elif image_type in rgb24_names:
            self.image_type = zwoasi.ASI_IMG_RGB24
        else:
            raise ValueError(
                "Image type must be either 'RAW8', 'RAW16', or 'RGB24'."
            )

    def set_exposure_setting(self, exposure_setting: int) -> None:
        self.exposure_setting = int(exposure_setting)  # [us]

    def set_gain_setting(self, gain_setting: int) -> None:
        self.gain_setting = gain_setting

    def _initalize_camera(self):
        self.camera_obj = zwoasi.Camera(0)
        self.camera_obj.set_control_value(0, self.gain_setting)
        self.camera_obj.set_control_value(1, self.exposure_setting)
        self.camera_obj.set_image_type(self.image_type)

    def _close_camera(self):
        self.camera_obj = None

    def liveview(self) -> None:
        """Enables liveview of the camera.

        Use :error:`KeyboardInterrupt` to end liveview.

        Uses existing :param:`gain_setting` and :param:`exposure_setting`.
        """
        self._initalize_camera()
        run_flag = True
        while run_flag:
            try:
                self._display_image(self.camera_obj.capture())
                print(
                    "To end liveveiw use the stop button on the tool bar above."
                )
                display.clear_output(wait=True)
            except KeyboardInterrupt:
                run_flag = False
                break
        self._close_camera()
        display.clear_output()

    def capture(
        self, display_image: bool = False
    ) -> npt.NDArray[np.uint8 | np.uint16]:
        """Captures an image as a numpy array.

        Uses :param:`gain_setting` and :param:`exposure_setting`.
        """
        self._initalize_camera()
        image = self.camera_obj.capture()
        self._close_camera()
        if display_image:
            self._display_image(image)
        return image

    def _display_image(self, image: npt.NDArray[np.uint8 | np.uint16]):
        fig = plt.figure(figsize=(10, 10))
        ax = plt.Axes(fig, [0.0, 0.0, 1.0, 1.0])
        ax.set_axis_off()
        fig.add_axes(ax)
        plt.imshow(image)
        plt.show()
