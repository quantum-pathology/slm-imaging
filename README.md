# SLM Imaging

* [Overview](#overview)
* [System requirements](#system-requirements)
* [Software requirements](#software-requirements)
* [Installation](#installation)
* [Usage](#usage)
* [Contact](#contact)


## Overview

**SLM Imaging** is a repository of python and jupyter files used to operate
the SLM imaging setup in RAC2 1121. The setup is operated using Thorlabs APT 
motor controlled rotation stages, HOLOEYE GAIA-2 SLM, ZWO ASI 1600MM camera. 


## System Requirements

- **Windows 10**. Windows 11 is **NOT** compatible with the SLM software 
(as of April 2023).
- A reasonably powerful GPU. In general, any discrete GPU with 4k output
released within the last decade should work. Integrated GPU's with 4k output
should also work, but YMMW. An Nvidia dGPU is strongly recommended, with Game
Ready Drivers.


## Software Requirements

- A working version of `git`. This can be installed from [here](https://git-scm.com/download/win). 
- HOLOEYE SLM Pattern Generator, GAIA-2 Configuration Manager (only 
needed for initial setup) and HOLOEYE Python SDK. 
- `Python 3.10+`
    - Non-conda version is recommended, installed from [python.org](https://www.python.org/downloads/). 
- Thorlabs legacy APT software, downloaded
[here](https://www.thorlabs.com/software_pages/ViewSoftwarePage.cfm?Code=APT).
Make sure you select the 64bit option (unless you are on a very old 32bit
machine, in which case you'll have many other issues...).
- Thorlabs optical power meter drivers, downloaded from [here]. There is a
driver switcher included, make sure the NI-VISA (PM100D) driver is selected and
not the TLPM driver.


## Installation

Inside the code blocks are all commands that can be run in your favourite shell.
Powershell or git-bash are recommended. 

Clone the repository. Alternatively, you can also download the folder as a zip
and extract it. Note that you must have credentials and permission to access to
this repository:

    git clone https://git.uwaterloo.ca/medical-tissue-imaging/slm-imaging.git

Enter the newly cloned repository:

    cd slm-imaging

Create a virtual Python environment:

    python -m venv .venvs

Note if you have multiple versions of python installed, it is recommended that
you use [py-launcher](https://docs.python.org/3/using/windows.html#launcher) to
manage and run the different versions of python. This is installed by default
from a standard python installation. For example, to specify the creation of a
virtual environment with Python 3.10:

    py -3.10 -m venv .venv

Activate the virtual environment. This is the command to use whenever you want 
to invoke this specific virtual environment in the terminal:

    .venv/Scripts/activate

You should now see `(.venv)` preceding the working directory in the terminal. 

Install required packages:

    pip install -r requirements.txt

To complete installation for the thorlabs_apt package, you must locate the
`APT.dll` in "APT installation path\APT Server". Typically this can be found in:
`C:\Program Files\Thorlabs\APT\APT Server\APT.dll`. Copy the file into your
working folder or in `.venv/Lib/site-packages/thorlabs_apt/`. Assuming it is in
the default installation location, you can run the following:

    cp 'C:\Program Files\Thorlabs\APT\APT Server\APT.dll' '.venv/Lib/site-packages/thorlabs_apt/'

Congratulations! You have completed the installation. Now you can open your 
favourite editor, and start running experiments! Just don't forget to select 
the virtual environment as the python kernel.

### Troubleshooting Installation

#### Execution Policy Error

If you get this error while trying to activate the venv, run in an elevated
privileges Powershell (as admin):

    Set-ExecutionPolicy RemoteSigned


## Starting Python

There are currently two tested environments to run the files in this repository:
[VS Code](https://code.visualstudio.com/) and [Jupyter](https://jupyter.org/),
specifically [Jupyter Lab](https://github.com/jupyterlab/jupyterlab). However,
any IDE capable of running Jupyter notebooks locally should also work, like
[PyCharm](https://www.jetbrains.com/pycharm/) or
[Spyder](https://www.spyder-ide.org/.).

### VS Code

[VS Code](https://code.visualstudio.com/) (short for Visual Studio Code)
is an open source text editor designed for editing code. There are numerous
extensions enabling support for different languages, including Python and
Jupyter notebooks. There is also built in git integration, intellisense support,
interactive debugging, among many other features that can be enabled using 
extensions. You can download it [here](https://code.visualstudio.com/Download).

You need to initially set it up with the necessary
[python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
and
[jupyter](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter)
extensions, sign in with git credentials, and to select the correct python
interpreter (for the workspace) and also the correct kernel for every notebook,
which should be `.venv.`. 

### Jupyter Lab

The workflow to setting up and using Jupyter Lab varies greatly depending on
whether you are using conda or pip (pypi). 

#### Conda

To install Jupyter Lab, run within the anaconda prompt:

    conda install -c conda-forge jupyterlab

To run, you must first navigate to the desired folder (using `cd`), typically
named `Python Scripts` on PushinGroup devices. This folder can be found within
the user's documents folder. On RAC2Legion, anaconda prompt is automatically
started in this location, and navigating is not required. Typically, on a
standard user installation, anaconda prompt launches in the user's home folder. 
Thus, this is most likely command that is needed (but do not copy blindly):

    cd "Documents\Python Scripts"
    
To start Jupyter Lab, run in the same anaconda prompt:

    jupyter lab

Jupyter Lab will automatically open in the browser.

#### Pip 

To install Jupyter Lab, within your venv, run:

    pip install jupyter lab

To run Jupyter Lab, within your venv, run:

    jupyter lab

Jupyter Lab will automatically open in the browser.


## Usage

There two files that you will use: 
* [imaging.ipynb](imaging.ipynb)
* [polarizer_alignment_test.ipynb](polarizer_alignment_test.ipynb)

### imaging.ipynb

The main file you will be working with to operate the SLM and collect
measurements. *This is a WIP and is **NOT** yet operational*.

#### SLM Pattern Generation Test

A test to show a fork grating on the SLM, with the function `show_fork_grating`.

### polarizer_alignment_test.ipynb

A notebook file used to help align the waveplates, polarizer, and analyzer. 
A power meter is required. 

#### Setup

These are the program setup cells. They include imports, loading of drivers and 
libraries, and custom functions. 

#### Polarizer & Analyzer Alignment Test

Run this block to see how well the polarizer and analyzer are aligned. This will
generate the optimal offset angle, an intensity heat map and their extinction
ratio.

#### Waveplate Circularity Test

A quick and useful test to measure the circularity of polarization in the beam, 
typically to characterize the alignment of a quarter waveplate and polarizer
pair (to normalize laser intensity as the polarizer rotates). 


## Troubleshooting

### The Kernel has Died Error Message

This is a common error caused by the Thorlabs APT motors. If they are not 
properly managed and closed, there will be a conflict with the Kinesis drivers.
To fix:

1. Open `Kinesis` application
2. Click Connect and load all available motors - you may need to do this
multiple times to ensure all of the motors are loaded
3. Disconnect all devices
4. Restart jupyter kernel
5. Run `Setup` cells - it should now work correctly

To prevent this problem in the future, make sure to run `apt.core._cleanup()` 
before restarting or killing the kernel.

### VisaIOError (Power Meter Errors)

This error is raised whenever python has trouble communicating with an IO device
using visa, which in most cases should be the power meter. Here are a couple
possible solutions:

1. Check that the power meter is plugged in correctly -  sometimes unplugging
   and plugging it back in is all that's necessary. 
2. Check that the power meter driver is NI-VISA (or PM100D) and not TLPM in the
   driver switcher software. 
3. Verify that the serial number of the power meter is correct. By default, the
   chosen id is `"USB0::0x1313::0x8078::P0024196::INSTR"`. If you use a
   different power meter, you will need to change this value to correctly
   reflect that device's id. You can find it by using the command



### Jupyter Lab is Unresponsive or Frozen

Sometimes the web client would hang, the kernel is stuck (i.e., it stays at "the
kernel is restarting"), the kernel is unable to start, etc... The best option
may be to restart Jupyter Lab entirely, as long as you do not have any sensitive
unsaved data. To restart Jupyter Lab:

1. Navigate to the terminal currently running the program (Anaconda Prompt if 
you are using Anaconda, or Powershell/Cmd otherwise).
2. Use key command `ctrl+c` to terminate the process.
3. You may need to press `ctrl+c` one or two more times to force end.
4. Press the up arrow key to load the last used command, which should be
   `jupyter lab`, and hit enter.
5. This will automatically reopen a new tab with Jupyter Lab loaded. If you did
not close the previous tab with Jupyter Lab, it is recommended that you close
the new tab. This is due to undesirable behaviour with file conflicts when
multiple tabs with Jupyter Lab are opened simultaneously. 


## Contact

You can reach me at [tbkhoo@uwaterloo.ca](mailto:tbkhoo@uwaterloo.ca) if you 
have any questions or concerns.
